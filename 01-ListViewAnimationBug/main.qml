import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2

Window {
    visible: true



    /*
     * This example demonstrates the animation glitch when inserting elements to the model.
     * In this example, items are inserted into the model each 200ms - this is done to force the problem to occur faster.
     * The glitch occurs when pressing the up/down arrow keys.
     */
    width: 300
    height: 500

    Rectangle {
        id: root
        width: 300
        height: 500
        property int count: 9


        ListModel {
            id: theModel

            ListElement { number: 0 }
            ListElement { number: 1 }
            ListElement { number: 2 }
            ListElement { number: 3 }
            ListElement { number: 4 }
            ListElement { number: 5 }
            ListElement { number: 6 }
            ListElement { number: 7 }
            ListElement { number: 8 }
            ListElement { number: 9 }
        }


        Timer{
            interval: 200
            running: true
            repeat:true
            triggeredOnStart: false
            onTriggered: {
                theModel.insert(0, {"number": ++root.count});
                theModel.append({"number": ++root.count });
            }
        }

        Rectangle {
            color: "green"
            x: 0
            y: 300
            height: 300
            width: parent.width

        }

        ListView {

            x: 70
            y: 300
            width: 50
            height: 300
            anchors.margins: 20
            anchors.bottomMargin: 80

            spacing: 5
            focus: true
            clip: true

            /* In this example default highlight move animation is used */
            highlightMoveVelocity: -1
            highlightMoveDuration: 1000

            highlightFollowsCurrentItem: true
            preferredHighlightBegin: 0
            highlightRangeMode: ListView.StrictlyEnforceRange

            interactive: true

            displayMarginBeginning: 100
            displayMarginEnd: 100


            model: theModel


            delegate: numberDelegate
        }

        Component {
            id: numberDelegate

            Rectangle {
                objectName: model.number

                width: 40
                height:  40

                color: "red"

                Text {
                    anchors.centerIn: parent
                    font.pixelSize: 10
                    text: number
                }

            }
        }
    }


}
